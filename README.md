# sql-query-builder
Simple tool for building basic SQL queries in Java with compilation time basic checks on syntax


Example of usage:

SqlQueryBuilder builder = SqlQuery.getBuilder();
builder.select("x").from("tableName").where("a", "b").where("c", "d");
System.out.println(builder.getQuery().toString());
