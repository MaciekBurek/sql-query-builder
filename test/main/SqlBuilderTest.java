package main;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Created by Maciek on 12.05.2018.
 */
class SqlBuilderTest {
    private SqlQueryBuilder builder = SqlQuery.getBuilder();

    @Test
    void selectAllFromTable() throws WrongQueryException {
        builder.select("*").from("tableName");

        Assertions.assertEquals("select * from tableName", builder.getQuery().toString());
    }

    @Test
    void select2ValuesFromTable() throws WrongQueryException {
        builder.select("x", "y").from("tableName");

        Assertions.assertEquals("select x, y from tableName", builder.getQuery().toString());
    }

    @Test
    void selectValueFromTableWhereAEqualsB() throws WrongQueryException {
        builder.select("x").from("tableName").where("a").eq("b");

        Assertions.assertEquals("select x from tableName where a = b", builder.getQuery().toString());
    }

    @Test
    void selectValueFromTableWhereAEqualsBAndCEqualsD() throws WrongQueryException {
        builder.select("x").from("tableName").where("a").eq("b").and("c").eq("d");

        Assertions.assertEquals("select x from tableName where a = b and c = d", builder.getQuery().toString());
    }

    @Test
    void deleteAllFromTable() throws WrongQueryException {
        builder.delete().from("tableName");

        Assertions.assertEquals("delete from tableName", builder.getQuery().toString());
    }

    @Test
    void deleteFromTableWhereAlikeBAndCEqualsD() throws WrongQueryException {
        builder.delete().from("tableName").where("a").like("b").and("c").eq("d");

        Assertions.assertEquals("delete from tableName where a like b and c = d", builder.getQuery().toString());
    }

    @Test
    void updateSetAB() throws WrongQueryException {
        builder.update("tableName").set("a", "b");

        Assertions.assertEquals("update tableName set a = b", builder.getQuery().toString());
    }

    @Test
    void updateSetABWhereCLikeDAndEEqualsF() throws WrongQueryException {
        builder.update("tableName").set("a", "b").where("c").like("d").and("e").eq("f");

        Assertions.assertEquals("update tableName set a = b where c like d and e = f", builder.getQuery().toString());
    }

    @Test
    void updateSetABCDWhereEEqualsF() throws WrongQueryException {
        builder.update("tableName").set("a", "b").set("c", "d").where("e").eq("f");

        Assertions.assertEquals("update tableName set a = b, c = d where e = f", builder.getQuery().toString());
    }

    @Test
    void insertAValuesB() throws WrongQueryException {
        builder.insertInto("tableName").values("a", "b");

        Assertions.assertEquals("insert into tableName (a) values (b)", builder.getQuery().toString());
    }

    @Test
    void insertABValuesCD() throws WrongQueryException {
        builder.insertInto("tableName").values("a", "c").values("b", "d");

        Assertions.assertEquals("insert into tableName (a, b) values (c, d)", builder.getQuery().toString());
    }

    @Test
    void selectWhereMultipleConditionsSeparatedByAnd() throws WrongQueryException {
        builder.select("a", "b").from("tableName").where("a").eq("b").and("c").like("d").and("e").like("f");

        Assertions.assertEquals("select a, b from tableName where a = b and c like d and e like f", builder.getQuery().toString());
    }

    @Test
    void selectWhereAGreaterThanB() throws WrongQueryException {
        builder.select("x").from("tableName").where("a").gt("b").and("c").eq("d");

        Assertions.assertEquals("select x from tableName where a > b and c = d", builder.getQuery().toString());
    }

    @Test
    void selectWhereALessThanB() throws WrongQueryException {
        builder.select("x").from("tableName").where("a").lt("b").and("c").like("d");

        Assertions.assertEquals("select x from tableName where a < b and c like d", builder.getQuery().toString());
    }

    @Test
    void selectDistinctColumns() throws WrongQueryException {
        builder.selectDistinct("a", "b").from("tableName");

        Assertions.assertEquals("select distinct a, b from tableName", builder.getQuery().toString());
    }

    @Test
    void orderByColumn() throws WrongQueryException {
        builder.select("a", "b").from("tableName").orderBy("c");

        Assertions.assertEquals("select a, b from tableName order by c", builder.getQuery().toString());
    }

    @Test
    void orderByMultipleColumns() throws WrongQueryException {
        builder.select("a", "b").from("tableName").where("f").eq("g").orderBy("c", "d").orderBy("e");

        Assertions.assertEquals("select a, b from tableName where f = g order by c, d, e", builder.getQuery().toString());
    }

    @Test
    void orderByDescending() throws WrongQueryException {
        builder.select("a", "b").from("tableName").orderByDescending("c", "d").orderBy("e", "f");

        Assertions.assertEquals("select a, b from tableName order by c desc, d desc, e, f", builder.getQuery().toString());
    }
}