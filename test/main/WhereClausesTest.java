package main;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Created by Maciek on 12.05.2018.
 */
class WhereClausesTest {
    private SqlQueryBuilder builder = SqlQuery.getBuilder();

    @Test
    void equalsTest() throws WrongQueryException {
        builder.select("*").from("tableName").where("a").eq("b");

        Assertions.assertEquals("select * from tableName where a = b", builder.getQuery().toString());
    }

    @Test
    void notEqualsTest() throws WrongQueryException {
        builder.select("*").from("tableName").where("a").notEq("b");

        Assertions.assertEquals("select * from tableName where a <> b", builder.getQuery().toString());
    }

    @Test
    void greaterThanTest() throws WrongQueryException {
        builder.select("*").from("tableName").where("a").gt("b");

        Assertions.assertEquals("select * from tableName where a > b", builder.getQuery().toString());
    }

    @Test
    void lessThanTest() throws WrongQueryException {
        builder.select("*").from("tableName").where("a").lt("b");

        Assertions.assertEquals("select * from tableName where a < b", builder.getQuery().toString());
    }

    @Test
    void greaterOrEqualThanTest() throws WrongQueryException {
        builder.select("*").from("tableName").where("a").geq("b");

        Assertions.assertEquals("select * from tableName where a >= b", builder.getQuery().toString());
    }

    @Test
    void lessOrEqualThanTest() throws WrongQueryException {
        builder.select("*").from("tableName").where("a").leq("b");

        Assertions.assertEquals("select * from tableName where a <= b", builder.getQuery().toString());
    }

    @Test
    void betweenTest() throws WrongQueryException {
        builder.select("*").from("tableName").where("a").between("b", "c");

        Assertions.assertEquals("select * from tableName where a between b and c", builder.getQuery().toString());
    }

    @Test
    void notBetweenTest() throws WrongQueryException {
        builder.select("*").from("tableName").where("a").notBetween("b", "c");

        Assertions.assertEquals("select * from tableName where a not between b and c", builder.getQuery().toString());
    }

    @Test
    void singleInTest() throws WrongQueryException {
        builder.select("*").from("tableName").where("a").in("b");

        Assertions.assertEquals("select * from tableName where a in (b)", builder.getQuery().toString());
    }

    @Test
    void multipleInTest() throws WrongQueryException {
        builder.select("*").from("tableName").where("a").in("b", "c", "d");

        Assertions.assertEquals("select * from tableName where a in (b, c, d)", builder.getQuery().toString());
    }

    @Test
    void andTest() throws WrongQueryException {
        builder.select("x").from("tableName").where("a").eq("b").and("c").like("d");

        Assertions.assertEquals("select x from tableName where a = b and c like d", builder.getQuery().toString());
    }

    @Test
    void orTest() throws WrongQueryException {
        builder.select("x").from("tableName").where("a").eq("b").or("c").like("d").and("e").eq("f");

        Assertions.assertEquals("select x from tableName where a = b or c like d and e = f", builder.getQuery().toString());
    }

    @Test
    void notTest() throws WrongQueryException {
        builder.select("x").from("tableName").where("a").not().eq("b").or("c").not().like("d").and("e").eq("f");

        Assertions.assertEquals("select x from tableName where not a = b or not c like d and e = f", builder.getQuery().toString());
    }
}
