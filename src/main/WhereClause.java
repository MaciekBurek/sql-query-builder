package main;

/**
 * Created by Maciek on 18.05.2018.
 */
class WhereClause {
    private AttributeValuePair attributeValuePair;
    private boolean condition;

    WhereClause(AttributeValuePair attributeValuePair, boolean condition) {
        this.attributeValuePair = attributeValuePair;
        this.condition = condition;
    }

    @Override
    public String toString() {
        return (condition ? "" : "not " ) + attributeValuePair;
    }
}
