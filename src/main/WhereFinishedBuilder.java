package main;

/**
 * Created by Maciek on 18.05.2018.
 */
public interface WhereFinishedBuilder extends OrderableBuilder {
    WhereBuilder and(String attribute);
    WhereBuilder or(String attribute);
}
