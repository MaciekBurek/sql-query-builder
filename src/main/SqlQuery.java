package main;

/**
 * Created by Maciek on 12.05.2018.
 */
public class SqlQuery {
    private String query;

    SqlQuery(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return query;
    }

    public static SqlQueryBuilder getBuilder() {
        return new SqlQueryBuilderImpl();
    }
}
