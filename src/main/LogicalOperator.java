package main;

/**
 * Created by Maciek on 18.05.2018.
 */
enum LogicalOperator {
    AND(" and "),
    OR(" or ");

    String value;

    public String toString() {
        return value;
    }

    LogicalOperator(String value) {
        this.value = value;
    }
}
