package main;

/**
 * Created by Maciek on 12.05.2018.
 */
public interface InsertBuilder {
    InsertBuilder values(String attribute, String value);
}
