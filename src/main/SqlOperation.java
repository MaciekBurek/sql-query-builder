package main;

/**
 * Created by Maciek on 12.05.2018.
 */
enum SqlOperation {
    DELETE("delete"),
    SELECT("select"),
    SELECT_DISTINCT("select distinct"),
    UPDATE("update"),
    INSERT_INTO("insert into");

    String value;

    SqlOperation(String value) {
        this.value = value;
    }
}
