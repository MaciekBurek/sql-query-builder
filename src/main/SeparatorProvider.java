package main;

/**
 * Created by Maciek on 18.05.2018.
 */
interface SeparatorProvider {
    String get() throws UnableToProvideSeparatorException;
}
