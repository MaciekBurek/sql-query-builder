package main;

/**
 * Created by Maciek on 18.05.2018.
 */
public class OrderByAttribute {
    private String attribute;
    private boolean isDescending;

    OrderByAttribute(String attribute, boolean isDescending) {
        this.attribute = attribute;
        this.isDescending = isDescending;
    }

    @Override
    public String toString() {
        return attribute + (isDescending ? " desc" : "");
    }
}
