package main;

/**
 * Created by Maciek on 12.05.2018.
 */
public interface WhereableBuilder extends OrderableBuilder {
    WhereBuilder where(String attribute);
}
