package main;

/**
 * Created by Maciek on 12.05.2018.
 */
public interface UpdateBuilder {
    SetBuilder set(String name, String value);
}
