package main;

/**
 * Created by Maciek on 12.05.2018.
 */
class AttributeValuePair {
    private String attribute;
    private String value;
    private String separator;

    AttributeValuePair(String attribute, String value, String separator) {
        this.attribute = attribute;
        this.value = value;
        this.separator = separator;
    }

    @Override
    public String toString() {
        return attribute + separator + value;
    }
}
