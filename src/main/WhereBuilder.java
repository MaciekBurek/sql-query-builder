package main;

/**
 * Created by Maciek on 12.05.2018.
 */
public interface WhereBuilder {
    WhereFinishedBuilder eq(String value);

    WhereFinishedBuilder like(String regex);

    WhereFinishedBuilder gt(String value);

    WhereFinishedBuilder lt(String value);

    WhereFinishedBuilder notEq(String value);

    WhereFinishedBuilder geq(String value);

    WhereFinishedBuilder leq(String value);

    WhereFinishedBuilder between(String min, String max);

    WhereFinishedBuilder notBetween(String min, String max);

    WhereFinishedBuilder in(String firstValue, String... followingValues);

    WhereBuilder not();
}
