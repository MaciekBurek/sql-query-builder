package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Maciek on 12.05.2018.
 */
class SqlQueryBuilderImpl implements FromableBuilder, WhereableBuilder, UpdateBuilder, WhereFinishedBuilder,
        FinishedBuilder, InsertBuilder, SetBuilder, SqlQueryBuilder {
    SqlOperation operation;
    String tableName;
    List<String> fields;
    List<String> insertedFields;
    List<String> values;
    List<WhereClause> whereClauses;
    List<LogicalOperator> whereLogicalOperators;
    List<AttributeValuePair> assignments;
    List<OrderByAttribute> orderByAttributes;

    SqlQueryBuilderImpl() {
        initialize();
    }

    public FromableBuilder select(String... fields) {
        initialize();
        operation = SqlOperation.SELECT;
        this.fields = Arrays.asList(fields);

        return this;
    }

    public FromableBuilder selectDistinct(String... fields) {
        initialize();
        operation = SqlOperation.SELECT_DISTINCT;
        this.fields = Arrays.asList(fields);

        return this;
    }

    public InsertBuilder insertInto(String tableName) {
        initialize();
        this.tableName = tableName;
        operation = SqlOperation.INSERT_INTO;

        return this;
    }

    public FromableBuilder delete() {
        initialize();
        operation = SqlOperation.DELETE;

        return this;
    }

    public UpdateBuilder update(String tableName) {
        initialize();
        this.tableName = tableName;
        operation = SqlOperation.UPDATE;

        return this;
    }

    public InsertBuilder values(String attribute, String value) {
        insertedFields.add(attribute);
        values.add(value);

        return this;
    }

    public WhereableBuilder from(String tableName) {
        this.tableName = tableName;

        return this;
    }

    public WhereBuilder and(String attribute) {
        whereLogicalOperators.add(LogicalOperator.AND);

        return where(attribute);
    }

    public WhereBuilder or(String attribute) {
        whereLogicalOperators.add(LogicalOperator.OR);

        return where(attribute);
    }

    public WhereBuilder where(String name) {
        return new WhereValueBuilder(this, true, name);
    }

    public OrderableBuilder orderBy(String... attributes) {
        return orderBy(attributes, false);
    }

    public OrderableBuilder orderByDescending(String... attributes) {
        return orderBy(attributes, true);
    }

    private OrderableBuilder orderBy(String[] attributes, boolean isDescending) {
        for(String attribute : attributes)
            orderByAttributes.add(new OrderByAttribute(attribute, isDescending));

        return this;
    }

    public SetBuilder set(String name, String value) {
        assignments.add(new AttributeValuePair(name, value, " = "));

        return this;
    }

    public SqlQuery getQuery() throws WrongQueryException {
        return new SqlQuery(new ToStringConverter(this).getQuery());
    }

    SqlQueryBuilderImpl addWhereClause(String attribute, String value, String separator, boolean condition) {
        whereClauses.add(new WhereClause(new AttributeValuePair(attribute, value, separator), condition));

        return this;
    }

    private void initialize() {
        fields = new ArrayList<>();
        insertedFields = new ArrayList<>();
        whereClauses = new ArrayList<>();
        whereLogicalOperators = new ArrayList<>();
        assignments = new ArrayList<>();
        values = new ArrayList<>();
        orderByAttributes = new ArrayList<>();
    }
}
