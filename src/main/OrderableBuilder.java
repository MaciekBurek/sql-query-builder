package main;

/**
 * Created by Maciek on 18.05.2018.
 */
public interface OrderableBuilder {
    OrderableBuilder orderBy(String... attributes);
    OrderableBuilder orderByDescending(String... attributes);
}
