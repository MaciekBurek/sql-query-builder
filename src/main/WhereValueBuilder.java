package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Maciek on 18.05.2018.
 */
class WhereValueBuilder implements WhereBuilder {
    private final String attribute;
    private SqlQueryBuilderImpl builder;
    private boolean condition;

    WhereValueBuilder(SqlQueryBuilderImpl builder, boolean condition, String attribute) {
        this.builder = builder;
        this.condition = condition;
        this.attribute = attribute;
    }

    public WhereFinishedBuilder eq(String value) {
        return builder.addWhereClause(attribute, value, " = ", condition);
    }

    public WhereFinishedBuilder like(String regex) {
        return builder.addWhereClause(attribute, regex, " like ", condition);
    }

    public WhereFinishedBuilder gt(String value) {
        return builder.addWhereClause(attribute, value, " > ", condition);
    }

    public WhereFinishedBuilder lt(String value) {
        return builder.addWhereClause(attribute, value, " < ", condition);
    }

    public WhereFinishedBuilder notEq(String value) {
        return builder.addWhereClause(attribute, value, " <> ", condition);
    }

    public WhereFinishedBuilder geq(String value) {
        return builder.addWhereClause(attribute, value, " >= ", condition);
    }

    public WhereFinishedBuilder leq(String value) {
        return builder.addWhereClause(attribute, value, " <= ", condition);
    }

    public WhereFinishedBuilder between(String min, String max) {
        return builder.addWhereClause(attribute, min + " and " + max, " between ", condition);
    }

    @Override
    public WhereFinishedBuilder notBetween(String min, String max) {
        return builder.addWhereClause(attribute, min + " and " + max, " not between ", condition);
    }

    @Override
    public WhereFinishedBuilder in(String firstValue, String... followingValues) {
        List<String> values = new ArrayList<>();
        values.add(firstValue);
        values.addAll(Arrays.asList(followingValues));

        try {
            return builder.addWhereClause(
                    attribute, "(" + ToStringConverter.getSeparatedValuesFromList(
                            new ConstantSeparatorProvider(", "), values) + ")", " in ", condition);
        } catch (WrongQueryException e) {
            //impossible to happen
            //todo: think what to do here

            return builder;
        }
    }

    public WhereBuilder not() {
        condition = false;

        return this;
    }
}
