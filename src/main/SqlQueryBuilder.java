package main;

/**
 * Created by Maciek on 12.05.2018.
 */
public interface SqlQueryBuilder {
    FromableBuilder select(String... fields);

    InsertBuilder insertInto(String tableName);

    FromableBuilder delete();

    UpdateBuilder update(String tableName);

    SqlQuery getQuery() throws WrongQueryException;

    FromableBuilder selectDistinct(String... fields);
}
