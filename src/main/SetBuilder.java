package main;

/**
 * Created by Maciek on 12.05.2018.
 */
public interface SetBuilder extends WhereableBuilder {
    SetBuilder set(String name, String value);
}
