package main;

/**
 * Created by Maciek on 18.05.2018.
 */
class ConstantSeparatorProvider implements SeparatorProvider {
    private final String separator;

    ConstantSeparatorProvider(String separator) {
        this.separator = separator;
    }

    @Override
    public String get() {
        return separator;
    }
}
