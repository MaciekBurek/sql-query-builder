package main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Maciek on 12.05.2018.
 */
class ToStringConverter {
    private SqlQueryBuilderImpl builder;
    private StringBuffer stringBuffer;

    ToStringConverter(SqlQueryBuilderImpl builder) {
        stringBuffer = new StringBuffer();
        this.builder = builder;
    }

    public String getQuery() throws WrongQueryException {
        stringBuffer = new StringBuffer(builder.operation.value + " ");

        if (builder.fields.size() > 0)
            appendFields();

        appendTableName();

        if (builder.insertedFields.size() > 0)
            appendInsertedFields();

        if (builder.values.size() > 0)
            appendValues();

        if (builder.assignments.size() > 0)
            appendAssignments();

        if (builder.whereClauses.size() > 0)
            appendWhereClauses();

        if (builder.orderByAttributes.size() > 0)
            appendOrderBy();

        return stringBuffer.toString();
    }

    private void appendTableName() {
        if (builder.operation != SqlOperation.UPDATE && builder.operation != SqlOperation.INSERT_INTO)
            stringBuffer.append("from ");

        stringBuffer.append(builder.tableName);
    }


    private void appendFields() throws WrongQueryException {
        stringBuffer.append(getSeparatedValuesFromList(new ConstantSeparatorProvider(", "), new ArrayList<>(builder.fields)));
        stringBuffer.append(" ");
    }

    private void appendInsertedFields() throws WrongQueryException {
        stringBuffer.append(" (");
        stringBuffer.append(getSeparatedValuesFromList(new ConstantSeparatorProvider(", "), new ArrayList<>(builder.insertedFields)));
        stringBuffer.append(")");
    }

    private void appendAssignments() throws WrongQueryException {
        stringBuffer.append(" set ");
        stringBuffer.append(getSeparatedValuesFromList(new ConstantSeparatorProvider(", "),
                builder.assignments.stream().map(AttributeValuePair::toString).collect(Collectors.toList())));
    }

    private void appendValues() throws WrongQueryException {
        stringBuffer.append(" values (");
        stringBuffer.append(getSeparatedValuesFromList(new ConstantSeparatorProvider(", "), new ArrayList<>(builder.values)));
        stringBuffer.append(")");
    }

    private void appendWhereClauses() throws WrongQueryException {
        stringBuffer.append(" where ");
        stringBuffer.append(getSeparatedValuesFromList(new ListSeparatorProvider(
                builder.whereLogicalOperators.stream().map(Enum::toString).collect(Collectors.toList())),
                builder.whereClauses.stream().map(WhereClause::toString).collect(Collectors.toList())));
    }

    private void appendOrderBy() throws WrongQueryException {
        stringBuffer.append(" order by ");
        stringBuffer.append(getSeparatedValuesFromList(new ConstantSeparatorProvider(", "),
                builder.orderByAttributes.stream().map(OrderByAttribute::toString).collect(Collectors.toList())));
    }

    static String getSeparatedValuesFromList(SeparatorProvider separatorProvider, final List<String> values) throws WrongQueryException {
        StringBuffer buffer = new StringBuffer();

        try {
            for (int i = 0; i < values.size(); i++) {
                if (i > 0)
                    buffer.append(separatorProvider.get());

                buffer.append(values.get(i));
            }
        } catch (UnableToProvideSeparatorException e) {
            throw new WrongQueryException();
        }

        return buffer.toString();
    }
}
