package main;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Maciek on 18.05.2018.
 */
class ListSeparatorProvider implements SeparatorProvider {
    private final Iterator<String> iterator;

    public ListSeparatorProvider(List<String> separators) {
        this.iterator = separators.iterator();
    }

    @Override
    public String get() throws UnableToProvideSeparatorException {
        if (iterator.hasNext())
            return iterator.next();

        throw new UnableToProvideSeparatorException();
    }
}
